const parseCookie = (cookie?: string): { [key: string]: string } => {
  const parsedCookie: { [key: string]: string } = {};

  if (cookie && cookie.split) {
    cookie.split("; ").forEach(c => {
      const [key, value] = c.split("=");
      parsedCookie[key] = value;
    });
  }

  return parsedCookie;
};

export default parseCookie;
