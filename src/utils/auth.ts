export const isAuthenticated = (cookie: { [key: string]: string }): boolean => {
  if (cookie && cookie["token"] && cookie["token"] === "mock-token") {
    console.log(`User Authenticated`);
    return true;
  }

  return false;
};
