import WebSocket from "ws";

class WebsocketSessionManager {
  private sessions: Map<string, WebSocket>;

  constructor() {
    this.sessions = new Map<string, WebSocket>();
  }

  addNewConnection(connId: string, socket: WebSocket): void {
    this.sessions.set(connId, socket);
  }

  removeConnection(connId: string): void {
    this.sessions.delete(connId);
  }

  getAllConnections(): Array<WebSocket> {
    const sockets = new Array<WebSocket>();

    const allSessions = this.sessions.values();
    for (const socket of allSessions) {
      sockets.push(socket);
    }

    return sockets;
  }

  getConnectionById(connId: string): WebSocket | undefined {
    return this.sessions.get(connId);
  }
}

export default new WebsocketSessionManager();
