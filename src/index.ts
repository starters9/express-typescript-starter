import "dotenv/config";
import { WebSocketServer } from "ws";
import { v4 } from "uuid";

import app from "./app";
import { configs } from "./configs/configUtil";
import { isAuthenticated } from "./utils/auth";
import wsSessionManager from "./utils/websocket";
import parseCookie from "./utils/cookie-parser";

console.log("nodeENV:", process.env.NODE_ENV);
console.log("configs:", configs);

const port = +(process.env.PORT || 8443);
const server = app().listen(port, () => {
  console.log(`Express app started on port ${port}`);
});

const wss = new WebSocketServer({ noServer: true });
wss.on("connection", (incomingSocket, request) => {
  const connectionId = v4().toString();

  /**
   * WS Closing Codes
   * https://developer.mozilla.org/en-US/docs/Web/API/CloseEvent/code
   */
  incomingSocket.on("close", (code, reason) => {
    wsSessionManager.removeConnection(connectionId);
    incomingSocket.send("Bye now");
    console.log(`Bye ${connectionId}`);
  });

  incomingSocket.on("message", data => {
    console.log(`${connectionId}: ${data}`);

    const allConnections = wsSessionManager.getAllConnections();
    allConnections.forEach(outgoingSocket => {
      outgoingSocket.send(`${connectionId} said ${data}`);
    });

    console.log(`relay message to ${allConnections.length} connections`);
  });

  wsSessionManager.addNewConnection(connectionId, incomingSocket);
  incomingSocket.send("Hello there!");
  console.log(`Hello ${connectionId}`);
});
console.log(`Websocket server started on port ${port}`);

server.on("upgrade", (req, socket, head) => {
  // make websocket connection if authenticated
  if (isAuthenticated(parseCookie(req.headers.cookie))) {
    wss.handleUpgrade(req, socket, head, client => {
      wss.emit("connection", client, req);
    });
  } else {
    req.destroy();
  }
});
