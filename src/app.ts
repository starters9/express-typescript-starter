import express from "express";
import helmet from "helmet";
import cookieParser from "cookie-parser";
import cors from "cors";

import appRoutes from "./routes/app-routes";
import { authenticate, authorize } from "./middlewares/auth";

const app = () => {
  const app = express();

  app.use(helmet());
  app.use(cookieParser());

  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));

  app.use(cors());

  app.get("/api/v1/ping", (req, res, next) => {
    res.send("Hello I am healthy!");
  });

  app.post("/api/v1/login", (req, res) => {
    if (req.body.password === "mock") {
      res.cookie("token", "mock-token", {
        httpOnly: true,
        sameSite: "none",
        maxAge: 3600 * 1000,
      });
      res.send("Cookie Set");
      return;
    }

    res.status(401).send();
  });

  app.use(authenticate);
  app.use(authorize);

  app.use("/api/v1", appRoutes());

  return app;
};

export default app;
