import path from "path";
import fs from "fs";

const pathToConfig = path.resolve(__dirname, `${process.env.NODE_ENV}.json`);
const configs = JSON.parse(fs.readFileSync(pathToConfig, { encoding: "utf-8" }));
console.info("\n\n~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~");
console.info(`Configurations are loaded for ${process.env.NODE_ENV}`);
console.info("~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~.~\n\n");

export { configs };
