import express from "express";

const appRotues = () => {
  const router = express.Router();

  router.post("/replay", (req, res) => {
    res.send(req.body);
  });

  return router;
};

export default appRotues;
