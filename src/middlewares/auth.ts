import { RequestHandler } from "express";
import { isAuthenticated } from "../utils/auth";

export const authenticate: RequestHandler = (req, res, next) => {
  if (isAuthenticated(req.cookies)) {
    next();
    return;
  }

  res.status(401).send("Not Authenticated");
};

export const authorize: RequestHandler = (req, res, next) => {
  next();
};
